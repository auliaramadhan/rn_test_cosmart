import { IWork } from '@/types/Booklistbysubject'
import { IUser } from '@/types/User'
import { createSlice } from '@reduxjs/toolkit'

const slice = createSlice({
  name: 'userList',
  initialState: [] as IUser[],
  reducers: {
    addUser: (state, action: Payload<IUser>) => {
      state.push(action.payload)
    },
    addBook: (state, action: Payload<{ user: IUser; book: IWork }>) => {
      const { book, user } = action.payload
      const index = state.findIndex(v => user.email === v.email)
      state[index].book.push(book)
    },
    removeBook: (state, action: Payload<{ user: IUser; indexBook: number }>) => {
      const { indexBook, user } = action.payload
      const index = state.findIndex(v => user.email === v.email)

      if (indexBook >= 0) {
        state[index].book.splice(indexBook, 1)
      }
      console.log(state[index].book.length)
      return state
    },
  },
})

export const { addUser, addBook, removeBook } = slice.actions

export default slice.reducer

type Payload<T> = {
  payload: T
  type: string
}