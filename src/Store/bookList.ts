import { bookApi } from '@/Services/modules/getBookSubject'
import { IBookListbySubject, IWork } from '@/types/Booklistbysubject'
import { IUser } from '@/types/User'
import { createSlice } from '@reduxjs/toolkit'

const slice = createSlice({
  name: 'userList',
  initialState: [] as IWork[],
  reducers: {
  },
  extraReducers(builder) {
    builder.addMatcher(
      bookApi.endpoints.getBookMystery.matchFulfilled,
      (state, { payload }: { payload: IBookListbySubject }) => {
        if (payload.request.offset + 10 >= state.length) {
          state.push(...payload.works)
        }
      })
  },
})

// export const {  } = slice.actions

export default slice.reducer

type Payload<T> = {
  payload: T
  type: string
}