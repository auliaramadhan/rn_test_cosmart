import { IUser } from '@/types/User'
import { createSlice } from '@reduxjs/toolkit'
import { addBook, addUser, removeBook } from './userList'

const slice = createSlice({
  name: 'auth',
  initialState: null as IUser | null,
  reducers: {
    login: (state, action: Payload) => {
      state = action.payload
      return state
    },
    logOut: (state, _) => {
      state = null
      return state
    },
  },
  extraReducers(builder) {
    builder.addCase(addBook, (state, action) => {
      state?.book.push(action.payload.book)
    })
    builder.addCase(removeBook, (state, action) => {
      const { indexBook, user } = action.payload
      if (indexBook >= 0) {
        state?.book?.splice(indexBook, 1)
        
      }
      // state = action.payload.user
      // return state
    })
    builder.addCase(addUser, (state, action) => {
      state = action.payload
      return state
    })
  },
})

export const { login, logOut } = slice.actions

export default slice.reducer

type Payload = {
  payload: IUser
  type: string
}
