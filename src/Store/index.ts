import AsyncStorage from '@react-native-async-storage/async-storage'
import { configureStore, combineReducers } from '@reduxjs/toolkit'
import { setupListeners } from '@reduxjs/toolkit/query'
import {
  persistReducer,
  persistStore,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
} from 'redux-persist'

import { api } from '@/Services/api'
import theme from './Theme'
import auth from './auth'
import userList from './userList'
import reactotron from 'reactotron-react-native'

const reducers = combineReducers({
  theme,
  auth,
  userList,
  api: api.reducer,
})

export type rootState = ReturnType<typeof reducers>

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['theme', 'auth', 'userList'],
}

const persistedReducer = persistReducer(persistConfig, reducers)

const ENHANCER = []
if (__DEV__) {
  ENHANCER.push(reactotron.createEnhancer!())
}

const store = configureStore({
  reducer: persistedReducer,
  // tambah ini runtuk reactotron redux
  enhancers: ENHANCER,
  middleware: getDefaultMiddleware => {
    const middlewares = getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
    }).concat(api.middleware)

    if (__DEV__ && !process.env.JEST_WORKER_ID) {
      const createDebugger = require('redux-flipper').default
      middlewares.push(createDebugger())
    }

    return middlewares
  },
})

const persistor = persistStore(store)

setupListeners(store.dispatch)

export { store, persistor }
