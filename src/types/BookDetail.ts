export interface IBookDetail {
  title: string
  key: string
  authors: IAuthor[]
  type: IType
  description: string
  covers: number[]
  subject_places: string[]
  subjects: string[]
  subject_people: string[]
  subject_times: string[]
  location: string
  latest_revision: number
  revision: number
  created: ICreated
  last_modified: ICreated
}

export interface IAuthor {
  author: IType
  type: IType
}

export interface IType {
  key: string
}

export interface ICreated {
  type: string
  value: Date
}
