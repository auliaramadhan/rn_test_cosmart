export interface IBookListbySubjectReq {
  offset: number
  limit: number
  subject?: string
}

export interface IBookListbySubject {
  key: string
  name: string
  subject_type: string
  work_count: number
  works: IWork[]
  request: IBookListbySubjectReq
}

export interface IWork {
  key: string
  title: string
  edition_count: number
  cover_id: number
  cover_edition_key: string
  subject: string[]
  ia_collection: string[]
  lendinglibrary: boolean
  printdisabled: boolean
  lending_edition: string
  lending_identifier: string
  authors: IAuthor[]
  first_publish_year: number
  ia: string
  public_scan: boolean
  has_fulltext: boolean
  availability: IAvailability
  returnDate: number | null
}

export interface IAuthor {
  key: string
  name: string
}

export interface IAvailability {
  status: string
  available_to_browse: boolean
  available_to_borrow: boolean
  available_to_waitlist: boolean
  is_printdisabled: boolean
  is_readable: boolean
  is_lendable: boolean
  is_previewable: boolean
  identifier: string
  isbn: null | string
  oclc: null
  openlibrary_work: string
  openlibrary_edition: string
  last_loan_date?: null | Date
  num_waitlist: null
  last_waitlist_date: null
  is_restricted: boolean
  is_browseable: boolean
  __src__: string
}
