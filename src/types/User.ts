import { IWork } from "./Booklistbysubject"

export interface IUser {
   email: string
   username: string
   password: string
   book: IWork[]
}