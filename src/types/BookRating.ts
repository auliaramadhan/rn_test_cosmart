export interface IBookRating {
   summary: ISummary;
   counts:  { [key: string]: number };
}

export interface ISummary {
   average:  number;
   count:    number;
   sortable: number;
}
