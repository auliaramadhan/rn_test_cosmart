import { API_URL } from '@/Config/api_url'
import { api } from '@/Services/api'
import { IBookDetail } from '@/types/BookDetail'
import { IBookListbySubject, IBookListbySubjectReq } from '@/types/Booklistbysubject'
import { IBookRating } from '@/types/BookRating'

export const bookApi = api.injectEndpoints({
  endpoints: build => ({
    getBook: build.query<IBookListbySubject, IBookListbySubjectReq>({
      query: data => ({
        url: API_URL.getSubject(data.subject!),
        params: data,
      }),
      transformResponse(baseQueryReturnValue: IBookListbySubject, meta, arg) {
        baseQueryReturnValue.request = arg
        return baseQueryReturnValue
      },
    }),
    getBookMystery: build.query<IBookListbySubject, IBookListbySubjectReq>({
      query: data => ({
        url: API_URL.getSubjectMystery,
        params: data,
      }),
      transformResponse(baseQueryReturnValue: IBookListbySubject, meta, arg) {
        baseQueryReturnValue.request = arg
        return baseQueryReturnValue
      },
    }),
    getBookDetail: build.query<IBookDetail, string>({
      query: kode => ({
        url: API_URL.detailBook(kode),
      }),
    }),
    getBookRating: build.query<IBookRating, string>({
      query: kode => ({
        url: API_URL.ratingsBook(kode),
      }),
    }),
  }),
  overrideExisting: false,
})

export const {
  useGetBookMysteryQuery,
  useGetBookQuery,
  useGetBookDetailQuery,
  useGetBookRatingQuery,
} = bookApi
