/**
 * This file contains the application's variables.
 *
 * Define color, sizes, etc. here instead of duplicating them throughout the components.
 * That allows to change them more easily later on.
 */

/**
 * Colors
 */
export const Colors = {
  // Example colors:
  transparent: 'rgba(0,0,0,0)',
  white: '#ffffff',
  text: '#212529',
  primary: '#E14032',
  primaryShadow: 'rgba(235, 87, 87, 0.2)',
  divider: '#ddd',
  border: '#483F53',
  borderList: '#F4F5F6',
  bglayout: '#F6F6F7',
  bglayout2: '#A6A9B010',
  loadingBg: '#EFEFEF',
  loadingHightlight: '#FBFBFB',
  greyDark: '#858585',
  blackText: '#333333',
  blackText60: '#33333360',

  // background
  background: '#1F0808',
  bgInput: '#F4F4F5',
  bgInput2: '#a6a9b012',
  bgPrimary: '#F4F4F5',
  bgYellow: '#FBEC97',
  bgModal: '#00000060',
  bgGrey: '#F4F4F5',
  bgBlack: '#151B26',
  bgForm: 'rgba(166, 169, 176, 0.12)',

  // normal color
  success: '#28a745',
  error: '#dc3545',
  warning: '#ffc107',
  info: '#0dcaf0',
  iconColor: '#A6A9B0',

  name: {
    royalBlue: '#00539CFF', 
    peach: '#EEA47FFF',
    blue: '#2F3C7E', 
    pink: '#FBEAEB',
    charcoal: '#101820FF', 
    yellow: '#FEE715FF',
    coral: '#F96167', 
    yellow: '#FCE77D',
    lime: '#CCF381',
    lavender: '#E2D1F9', 
    teal: '#317773',
    cherryRed: '#990011FF', 
    offWhite: '#FCF6F5FF',
    babyBlue: '#8AAAE5',
    white: '#FFFFFF',
    peachLight: '#FCEDDA', 
    burntOrange: '#EE4E34',
    lightBlue: '#ADD8E6', 
    darkBlue: '#00008b',
    forestGreen: '#2C5F2D', 
    mossGreen: '#97BC62FF',
    beige: '#DDC3A5', 
    blackBrown: '#201E20', 
    tan: '#E0A96D',
    blue: '#408EC6', 
    maroon: '#7A2048', 
    indigo: '#1E2761',
    scarlet: '#B85042', 
    lightOlive: '#E7E8D1', 
    lightTeal: '#A7BEAE',

    // mine
    black: '#000000',
    greenAlt: '#14AA2B',
    yellow: '#F2994A',
    grey: '#A6A9B0',
    dark: '#222222',
    light: '#eeeeee',
    green: '#0FBF76',
    greenPalma: '#14AA2B',
    orange: '#F2994A',
    softOrange: '#FFBE5C',
  },
  // color text
  colorText: {
    primary: '#333',
    secondary: '#8a8d8e',
    black: '#333',
    white: '#eee',
    grey: '#8a8d8e',
  },
}

export const NavigationColors = {
  primary: Colors.primary,
}

/**
 * FontSize
 */
export const FontSize = {
  body : 14,
  // small: 16,
  // regular: 20,
  large: 40,
  h1: 38,
  h2: 34,
  h3: 30,
  h4: 26,
  h5: 20,
  h6: 19,
  title: 24,
  input: 18,
  regular: 17,
  medium: 14,
  small: 12,
  tiny: 10,
  tiny8: 8,
  tiny10: 10,
  text8: 8,
  text11: 11,
  text12: 12,
  text13: 13,
  text14: 14,
  text16: 16,
  text18: 18,
  text20: 20,
  text22: 22,
  text24: 24,
  text26: 26,
}

/**
 * Metrics Sizes
 */
const tiny = 5 // 10
const small = tiny * 2 // 10
const regular = tiny * 3 // 15
const large = regular * 2 // 30
export const MetricsSizes = {
  tiny,
  small,
  regular,
  large,
}

const Variables = {
  Colors,
  NavigationColors,
  FontSize,
  MetricsSizes,
}

export default Variables
