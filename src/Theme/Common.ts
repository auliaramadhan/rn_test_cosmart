/**
 * This file defines the base application styles.
 *
 * Use it to define generic component styles (e.g. the default text styles, default button styles...).
 */
import { StyleSheet, TextStyle, ViewStyle, ImageStyle } from 'react-native'
import buttonStyles from './components/Buttons'
import { CommonParams } from './theme'

type ICommon = {
  [k in string]: ViewStyle | TextStyle | ImageStyle | ((data: any) => ViewStyle | TextStyle | ImageStyle)
}

export default function <C>({ Colors, ...args }: CommonParams<C>) {
  // const commont : ICommon = {
  const commont = {
    button: buttonStyles({ Colors, ...args }),
    circle: (radius: number) => ({
      borderRadius: radius,
      height: radius,
      width: radius,
    } as ViewStyle),

    backgroundPrimary: {
      backgroundColor: Colors.bgPrimary,
    },
    backgroundReset: {
      backgroundColor: Colors.transparent,
    },
    textInput: {
      borderWidth: 1,
      borderColor: Colors.text,
      backgroundColor: Colors.bgInput,
      color: Colors.text,
      minHeight: 50,
      textAlign: 'center',
      marginTop: 10,
      marginBottom: 10,
    },
    cardBorder: {
      borderWidth: 1,
      borderColor: Colors.divider,
      // backgroundColor: 'white',
      borderRadius: 8,
    },
    topRadius: (radius: number = 8) => ({
      borderTopLeftRadius: radius,
      borderTopRightRadius: radius,
    }),
    bottomRadius: (radius: number = 8) => ({
      borderBottomLeftRadius: radius,
      borderBottomRightRadius: radius,
    }),
    fab: {
      position: 'absolute',
      zIndex: 10,
      bottom: 16,
      right: 16,
    },
    modalContainer: {
      backgroundColor: 'white',
      position: 'absolute',
      // flex: 1,
      bottom: 0,
      width: '100%',
      borderTopLeftRadius: 30,
      borderTopRightRadius: 30,
    },
    modalDragger: {
      borderWidth: 2,
      borderColor: '#C0C0C0',
      height: 1,
      marginHorizontal: 150,
      marginVertical: 10,
      borderRadius: 90,
    },
    headerIcon: { height: 24, width: 24 },
  } satisfies ICommon
  return commont
}
