import React from 'react'
import { SafeAreaView, StatusBar } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack'
import { NavigationContainer } from '@react-navigation/native'
import { ExampleContainer, StartupContainer } from '@/Containers'
import { useTheme } from '@/Hooks'
import { MainStack, navigationRef } from './utils'
import AuthNav from './authNav'
import HomeScreen from '@/Containers/HomeScreen/HomeScreen'
import BookDetailScreen from '@/Containers/BookDetailScreen/BookDetailScreen'
import UserListScreen from '@/Containers/UserListScreen/UserListScreen'
import UserBookListScreen from '@/Containers/UserBookListScreen/UserBookListScreen'

// @refresh reset
const ApplicationNavigator = () => {
  const { Layout, darkMode, NavigationTheme } = useTheme()
  const { colors } = NavigationTheme

  return (
    <SafeAreaView style={[Layout.fill, { backgroundColor: colors.card }]}>
      <NavigationContainer theme={NavigationTheme} ref={navigationRef}>
        <StatusBar
          barStyle={darkMode ? 'light-content' : 'dark-content'}
          // TODO 
          // backgroundColor={backgroundStyle.backgroundColor}
        />
        <MainStack.Navigator screenOptions={{}}>

          <MainStack.Screen 
            name="Startup" 
            options={{
              headerShown: false
            }}
            component={StartupContainer} 
          />
          {AuthNav}
          <MainStack.Screen
            name="Main"
            component={HomeScreen}
            options={{
              animationEnabled: false,
            }}
          />
          <MainStack.Screen
            name="BookDetail"
            component={BookDetailScreen}
            options={{
              animationEnabled: false,
            }}
          />
          <MainStack.Screen
            name="UserList"
            component={UserListScreen}
            options={{
              animationEnabled: false,
            }}
          />
          <MainStack.Screen
            name="UserBookListScreen"
            component={UserBookListScreen}
            options={{
              animationEnabled: false,
            }}
          />
          <MainStack.Screen
            name="Example"
            component={ExampleContainer}
            options={{
              animationEnabled: false,
            }}
          />
        </MainStack.Navigator>
      </NavigationContainer>
    </SafeAreaView>
  )
}

export default ApplicationNavigator
