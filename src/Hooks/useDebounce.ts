/* eslint-disable react-hooks/exhaustive-deps */
import { EffectCallback, useEffect } from 'react'
import useTimeout from './useTimeout'

export default function useDebounce(
  callback: EffectCallback,
  delay: number,
  dependencies: any[] = [],
) {
  const { reset, clear } = useTimeout(callback, delay)
  useEffect(reset, [...dependencies, reset])
  useEffect(clear, [])
}
