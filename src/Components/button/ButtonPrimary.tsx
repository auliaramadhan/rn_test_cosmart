import { useTheme } from '@/Hooks'
import React, { ReactNode } from 'react'
import {
  TouchableOpacity,
  View,
  Text,
  StyleSheet,
  TouchableOpacityProps,
  TextStyle,
  ViewStyle,
  ActivityIndicator,
} from 'react-native'

type IProp = TouchableOpacityProps & {
  text: string
  children?: ReactNode
  textStyle?: TextStyle
  buttonStyle?: ViewStyle
  disabled?: boolean
  isLoading?: boolean
  revert?: boolean
}

const ButtonPrimary: React.FC<IProp> = (props: IProp) => {
  const {
    text,
    children,
    textStyle,
    buttonStyle,
    disabled,
    isLoading,
    revert = false,
    ...arg
  } = props
  function getText() {
    const buttonText = text || children || ''
    return buttonText
  }

  const { Colors, Fonts, Layout, Common, darkMode } = useTheme()

  const getButtonbackground = () => {
    if (revert) {
      return '#fff'
    }
    if (disabled) {
      return '#A6A9B0'
    }
    return Colors.primary
  }

  const getBordercolor = () => {
    if (revert) {
      return Colors.primary
    }
    if (disabled) {
      return '#A6A9B0'
    }
    return '#fff'
  }

  return (
    <TouchableOpacity disabled={disabled} {...arg}>
      <View
        style={[
          {
            backgroundColor: getButtonbackground(),
            borderColor: getBordercolor(),
            borderWidth: 1,
            height: 55,
            borderRadius: 100,
            justifyContent: 'center',
            alignItems: 'center',
            elevation: 0,
            ...buttonStyle,
          },
        ]}
      >
        {isLoading ? (
          <ActivityIndicator color={Colors.white} />
        ) : (
          <Text
            numberOfLines={1}
            style={[
              Fonts.bodyBold,
              { color: '#fff', textAlign: 'auto' },
              textStyle,
            ]}
          >
            {getText()}
          </Text>
        )}
      </View>
    </TouchableOpacity>
  )
}

ButtonPrimary.defaultProps = {
  disabled: false,
  isLoading: false,
}

export default ButtonPrimary
