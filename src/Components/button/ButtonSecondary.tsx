import { useTheme } from '@/Hooks'
import React, { ReactNode } from 'react'
import {
  TouchableOpacity,
  View,
  Text,
  StyleSheet,
  TouchableOpacityProps,
  TextStyle,
  ViewStyle,
  ActivityIndicator,
} from 'react-native'

type IProp = TouchableOpacityProps & {
  text: string
  children?: ReactNode
  textStyle?: TextStyle
  buttonStyle?: ViewStyle
  disabled?: boolean
  isLoading?: boolean
  revert?: boolean
}

const ButtonSecondary: React.FC<IProp> = (props: IProp) => {
  const {
    text,
    children,
    textStyle,
    buttonStyle,
    disabled,
    isLoading,
    revert = false,
    ...arg
  } = props
  function getText() {
    const buttonText = text || children || ''
    return buttonText
  }

  const {Colors, Fonts, Layout, Common, Images, darkMode } = useTheme()


  const getButtonbackground = () => {
    if (revert) {
      return '#fff'
    }
    if (disabled) {
      return '#A6A9B0'
    }
    return Colors.bgPrimary
  }

  const getBordercolor = () => {
    if (revert) {
      return Colors.primary
    }
    if (disabled) {
      return '#A6A9B0'
    }
    return Colors.divider
  }

  return (
    <TouchableOpacity disabled={disabled} {...arg}>
      <View
        style={[
          // eslint-disable-next-line react-native/no-inline-styles
          {
            backgroundColor: getButtonbackground(),
            borderColor: getBordercolor(),
            borderWidth: 4,
            height: 55,
            borderRadius: 100,
            justifyContent: 'center',
            elevation: 0,
            ...buttonStyle,
          },
        ]}
      >
        {isLoading ? (
          <ActivityIndicator color={Colors.blackText} />
        ) : (
          <Text numberOfLines={1} style={[Fonts.bodyBold, Fonts.textCenter, textStyle]}>
            {getText()}
          </Text>
        )}
      </View>
    </TouchableOpacity>
  )
}

ButtonSecondary.defaultProps = {
  disabled: false,
  isLoading: false,
}

export default ButtonSecondary

