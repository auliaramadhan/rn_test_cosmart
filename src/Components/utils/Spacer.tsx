import React from 'react'
import { View } from 'react-native'

interface Iprop {
  height?: number
  width?: number
  flex?: number
}

const Spacer : React.FC<Iprop> = ({ height, width, flex = 0 }: Iprop) => {
  return <View style={{ height, width, flex }} />
}

export default Spacer
