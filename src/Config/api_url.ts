const BASE_API_URL = 'https://openlibrary.org/'

export const API_URL = {
  detailBook: (kode: string) => `${BASE_API_URL}works/${kode}.json`,
  ratingsBook: (kode: string) => `${BASE_API_URL}works/${kode}/ratings.json`,
  getSubject: (subjects: string) => `${BASE_API_URL}subjects/${subjects}.json`,
  getSubjectMystery: `${BASE_API_URL}subjects/mystery.json`,
  getImg: (kode: string) => `https://covers.openlibrary.org/b/id/${kode}-M.jpg`,
}
