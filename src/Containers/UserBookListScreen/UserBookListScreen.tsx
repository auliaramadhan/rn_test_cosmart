/* eslint-disable react-native/no-inline-styles */
import React, { useEffect } from 'react'
import { View, SafeAreaView, Text, FlatList, Image } from 'react-native'
import { useTheme } from '@/Hooks'
import { Gutter } from '@/Theme/Gutters'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { RootStackScreenProps } from '../../Navigators/utils'
import { API_URL } from '@/Config/api_url'

const UserBookListScreen: React.FC<RootStackScreenProps<'Main'>> = ({
  navigation,
  route,
}) => {
  const { Common, Fonts, Layout, Colors } = useTheme()

  useEffect(() => {
    navigation.setOptions({
      title: route.params?.user.username,
    })
  }, [])

  return (
    <SafeAreaView style={[Layout.fill, Common.backgroundPrimary]}>
      {/* <Image source={Images.bgEllipse} style={styles.overlay} /> */}
      <View style={[Layout.fill, Gutter.padding16, Gutter.padding4T]}>
        {/* <Text>{JSON.stringify(route.params?.user)}</Text> */}
        <FlatList
          // ScrollIndicator={false}
          data={route.params?.user.book ?? []}
          numColumns={2}
          onEndReachedThreshold={0.2}
          keyExtractor={(_, index) => index.toString()}
          renderItem={({ index, item }) => (
            <View style={[Layout.fill, Gutter.margin4, {maxWidth: Layout.getNumberWidth / 2}]}>
              <TouchableOpacity
                style={[{ height: 300 }]}
                onPress={() => {
                  navigation.push('BookDetail', {
                    book: item,
                    user: route.params?.user,
                  })
                }}
              >
                <View style={[Common.cardBorder, Layout.fill]}>
                  <Image
                    source={{
                      uri: API_URL.getImg(item.cover_id.toString()),
                      height: 200,
                    }}
                    style={[Common.topRadius(8)]}
                    resizeMode="stretch"
                  />
                  <View
                    style={[Layout.colVCenter, Gutter.padding12, Layout.fill]}
                  >
                    <Text style={[Fonts.title18]} numberOfLines={3}>{item.title}</Text>
                    <Text style={[Fonts.caption10]}>
                      Publish on {item.first_publish_year}
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
          )}
        />
      </View>
    </SafeAreaView>
  )
}

export default UserBookListScreen
