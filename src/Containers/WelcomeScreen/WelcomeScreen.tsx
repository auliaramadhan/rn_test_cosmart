import React, { useState } from 'react'
import { View, Text, SafeAreaView, ScrollView } from 'react-native'
import { useDispatch } from 'react-redux'
import { useTranslation } from 'react-i18next'
import { ButtonPrimary, ButtonSecondary, InputBorder, Spacer } from '@/Components'
import { useTheme } from '@/Hooks'
import TextButton from '@/Components/button/ButtonText'
import { Gutter } from '@/Theme/Gutters'
import { RootStackScreenProps } from '../../Navigators/utils'
import Snackbar from 'react-native-snackbar'
import { login, logOut } from '@/Store/auth'
import MaterialIcons from '@/Components/MaterialIcons'

const WelcomeScreen: React.FC<RootStackScreenProps<'WelcomePage'>> = ({
  navigation,
  route,
}) => {
  const { t } = useTranslation()
  const { Common, Fonts, Gutters, Layout, Colors, darkMode } = useTheme()

  const dispatch = useDispatch()

  return (
    <SafeAreaView style={[Layout.fill]}>
      {/* <Spacer height={20} /> */}
      <View style={[Layout.fill, Common.backgroundPrimary]}>
        <ScrollView contentContainerStyle={[Gutters.padding16]}>
          <Spacer height={20} />
          <View>
            <Text style={[Fonts.title, Gutters.margin16B]}>Welcome</Text>
            <Text style={[Fonts.subtitle]}>Apakah anda member atau librarian</Text>
            <Spacer height={24} />
            <ButtonPrimary
              text="MEMBER"
              onPress={() => {
                navigation.push('Login')
              }}
            />
            <Spacer height={48} />
            <ButtonSecondary
              text="LIBRARIAN"
              onPress={() => {
                dispatch(logOut(''))
                navigation.push('UserList')
              }}
            />
            <Spacer height={12} />
          </View>
        </ScrollView>
      </View>
    </SafeAreaView>
  )
}

export default WelcomeScreen
