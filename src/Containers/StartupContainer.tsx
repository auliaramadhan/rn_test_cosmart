import React, { useEffect } from 'react'
import { ActivityIndicator, View, Text, Image } from 'react-native'
import { useTranslation } from 'react-i18next'
import { useTheme } from '@/Hooks'
import { setDefaultTheme } from '@/Store/Theme'
import { navigateAndSimpleReset } from '@/Navigators/utils'
import { Images } from '@/Theme/Images'
import { RootStackScreenProps } from '../Navigators/utils'

const StartupContainer: React.FC<RootStackScreenProps<'Startup'>> = ({
  navigation,
  route,
}) => {
  const { Layout, Gutters, Fonts } = useTheme()

  const { t } = useTranslation()
  const init = async () => {
    setDefaultTheme({ darkMode: false })
    await new Promise(resolve =>
      setTimeout(
        () => {
          navigateAndSimpleReset('WelcomePage')
          resolve(true)
        },
        __DEV__ ? 100 : 2000,
      ),
    )
  }

  useEffect(() => {
    init()
  })

  return (
    <View style={[Layout.fill, Layout.colCenter]}>
      <ActivityIndicator size={'large'} style={[Gutters.margin12V]} />
      <Image
        style={[Layout.fullHeight, Layout.fullWidth]}
        source={Images.logo}
        resizeMode="contain"
      />
      <Text style={[Fonts.textCenter, Fonts.title]}>{t('welcome')}</Text>
    </View>
  )
}

export default StartupContainer
