import React from 'react'
import { View, SafeAreaView, Text, FlatList, Image } from 'react-native'
import { useTheme } from '@/Hooks'
import { Gutter } from '@/Theme/Gutters'
import MaterialIcons from '@/Components/MaterialIcons'
import { useSelector } from 'react-redux'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { RootStackScreenProps } from '../../Navigators/utils'
import { rootState } from '@/Store'
import { IBookListbySubject } from '@/types/Booklistbysubject'
import { API_URL } from '@/Config/api_url'
import { IUser } from '@/types/User'
import { Spacer } from '@/Components'

const UserListScreen: React.FC<RootStackScreenProps<'UserList'>> = ({
  navigation,
  route,
}) => {
  const { Common, Fonts, Layout, Colors } = useTheme()

  const userList = useSelector((state: rootState) => state.userList)

  return (
    <SafeAreaView style={[Layout.fill, Common.backgroundPrimary]}>
      {/* <Image source={Images.bgEllipse} style={styles.overlay} /> */}
      <View style={[Layout.fill, Gutter.padding16, Gutter.padding4T]}>
        <FlatList
          showsVerticalScrollIndicator={false}
          data={userList}
          ItemSeparatorComponent={() => <Spacer height={8} />}
          renderItem={({ index, item }) => (
            <TouchableOpacity onPress={() => { 
              navigation.push('UserBookListScreen', {user: item})
            }}>
              <View
                style={[Common.cardBorder, Gutter.padding12, Layout.rowHCenter]}
              >
                <View
                  style={[
                    Common.circle(60),
                    Layout.center,
                    { backgroundColor: Colors.name.green },
                  ]}
                >
                  <Text style={[Fonts.h3, Fonts.white]}>
                    {item.username.charAt(0)}
                  </Text>
                </View>

                <Spacer width={8} />
                <View style={[Layout.colHCenter, Gutter.padding12]}>
                  <Text style={[Fonts.title18]}>{item.username}</Text>
                  <Text style={[Fonts.caption12]}>{item.email}</Text>
                </View>

                <Spacer flex={1} />
                <Text style={[Fonts.title18, Fonts.textCenter]}>
                  Book Lent{'\n'}
                  {item.book.length}
                </Text>
              </View>
            </TouchableOpacity>
          )}
        />
      </View>
    </SafeAreaView>
  )
}

export default UserListScreen
