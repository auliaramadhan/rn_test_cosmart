import React, { useState } from 'react'
import {
  View,
  ScrollView,
  SafeAreaView,
  Text,
  Image,
  Pressable,
  FlatList,
} from 'react-native'
import { useDispatch } from 'react-redux'
import { useTheme } from '@/Hooks'
import { changeTheme, ThemeState } from '@/Store/Theme'
import { logOut } from '@/Store/auth'
import { Gutter } from '@/Theme/Gutters'
import { RootStackScreenProps } from '@/Navigators/utils'
import { ButtonPrimary, Spacer } from '@/Components'
import { API_URL } from '@/Config/api_url'
import MaterialIcons from '@/Components/MaterialIcons'
import DateTimePickerModal from 'react-native-modal-datetime-picker'
import dayjs from 'dayjs'
import {
  useGetBookDetailQuery,
  useGetBookRatingQuery,
} from '@/Services/modules/getBookSubject'
import { addBook, removeBook } from '@/Store/userList'
import { useSelector } from 'react-redux'
import { rootState } from '@/Store'
import { IWork } from '@/types/Booklistbysubject'

const BookDetailScreen: React.FC<RootStackScreenProps<'BookDetail'>> = ({
  navigation,
  route,
}) => {
  const { Common, Fonts, Layout, Colors, darkMode } = useTheme()

  const [numLine, setNumLine] = useState(3)
  const dispatch = useDispatch()
  const storeUser = useSelector((state: rootState) => state.auth)
  const currentUser = route.params.user ?? storeUser

  const idBook = route.params.book.availability.openlibrary_work
  const queryRating = useGetBookRatingQuery(idBook)
  const querybook = useGetBookDetailQuery(idBook)
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false)
  const rating = queryRating.data?.summary.average ?? 0

  const indexBook =
    currentUser?.book.findIndex(
      v => v.availability.openlibrary_work === idBook,
    ) ?? -1


  return (
    <SafeAreaView style={[Layout.fill, Common.backgroundPrimary]}>
      {/* <Image source={Images.bgEllipse} style={styles.overlay} /> */}
      <View style={Layout.fill}>
        <ScrollView contentContainerStyle={[Gutter.padding16]}>
          {/* <Text>{JSON.stringify(querybook.data?.description)}</Text> */}

          <FlatList
            data={querybook.data?.covers ?? []}
            horizontal
            snapToAlignment="start"
            decelerationRate={'fast'}
            snapToInterval={Layout.getNumberWidth - 32}
            renderItem={({ index, item }) => (
              <Image
                source={{
                  uri: API_URL.getImg(item.toString()),
                  height: 400,
                  width: Layout.getNumberWidth - 32,
                }}
                resizeMode="stretch"
                style={[Common.cardBorder]}
              />
            )}
          />
          <View style={[Layout.colCenter]}>
            <Text style={Fonts.titleBold}>{querybook.data?.title}</Text>
            {route.params.book.authors.map(author => (
              <Text style={Fonts.title16}>{author.name} </Text>
            ))}
            <View style={[Layout.rowCenter]}>
              {Array(5)
                .fill(0)
                .map((_, index) => (
                  <MaterialIcons
                    name={
                      index < rating && rating < index + 1 && rating % 1 > 0.5
                        ? 'star-half'
                        : 'star'
                    }
                    size={24}
                    color={
                      Math.round(rating) >= index + 1
                        ? Colors.name.yellow
                        : Colors.name.grey
                    }
                  />
                ))}
            </View>
            <Spacer height={8} />
            <Text style={Fonts.body}>
              Revision Number: {querybook.data?.revision}
            </Text>
            <Spacer height={8} />
            <Text style={Fonts.body}>
              Publish date: {route.params.book.first_publish_year}
            </Text>
            <Spacer height={8} />
            {indexBook >= 0 && (
              <Text style={Fonts.body}>
                Return date:{' '}
                {dayjs(currentUser?.book[indexBook]?.returnDate).format('DD MMMM YYYY')} (
                {dayjs(currentUser?.book[indexBook]?.returnDate).diff(Date.now(), 'days')}{' '}
                Days Left)
              </Text>
            )}
            <Spacer height={8} />

            <Pressable
              onPress={() => {
                setNumLine(v => (v === 3 ? 0 : 3))
              }}
            >
              <Text
                style={[Fonts.body]}
                ellipsizeMode="tail"
                numberOfLines={numLine}
              >
                Subject: {'\n'}
                {querybook.data?.subjects?.map(subject => (
                  <>
                    <View style={[Common.cardBorder, Gutter.padding4]}>
                      <Text>{subject}</Text>
                    </View>{' '}
                  </>
                ))}
              </Text>
            </Pressable>

            <Spacer height={8} />
            {
              querybook.data?.description &&
              <View style={[Common.cardBorder, Gutter.padding8]}>
                <Text style={Fonts.body}>{querybook.data?.description}</Text>
              </View>
            }
          </View>
        </ScrollView>
        {
          storeUser &&
          <ButtonPrimary
            style={[Gutter.margin16]}
            text={indexBook >= 0 ? 'Return Book' : 'Pick Book'}
            onPress={() => {
              console.log(indexBook)
              if (indexBook >= 0) {
                dispatch(
                  removeBook({
                    indexBook: indexBook,
                    user: currentUser!,
                  }),
                )
              } else {
                setDatePickerVisibility(true)
              }
            }}
          />
        }
      </View>
      <DateTimePickerModal
        isVisible={isDatePickerVisible}
        mode="date"
        minimumDate={new Date(Date.now())}
        onConfirm={date => {
          setDatePickerVisibility(false)
          const book = route.params.book
          const newBook = Object.assign<IWork, IWork, IWork>({}, book, {
            returnDate: dayjs(date).valueOf(),
          })
          dispatch(
            addBook({
              book: newBook,
              user: currentUser!,
            }),
          )
        }}
        onCancel={() => {
          setDatePickerVisibility(false)
        }}
      />
    </SafeAreaView>
  )
}

export default BookDetailScreen
