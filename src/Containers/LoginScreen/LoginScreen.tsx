import React, { useState } from 'react'
import { View, Text, SafeAreaView, ScrollView } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { useTranslation } from 'react-i18next'
import { ButtonPrimary, InputBorder, Spacer } from '@/Components'
import { useTheme } from '@/Hooks'
import TextButton from '@/Components/button/ButtonText'
import { Gutter } from '@/Theme/Gutters'
import {
  navigateAndReset,
  navigateAndSimpleReset,
  RootStackScreenProps,
} from '@/Navigators/utils'
import Snackbar from 'react-native-snackbar'
import { login } from '@/Store/auth'
import MaterialIcons from '@/Components/MaterialIcons'
import { rootState } from '@/Store'
import { IUser } from '@/types/User'
import { addUser } from '@/Store/userList'

const LoginScreen: React.FC<RootStackScreenProps<'Login'>> = ({
  navigation,
  route,
}) => {
  const { t } = useTranslation()
  const { Common, Fonts, Gutters, Layout, Colors, darkMode } = useTheme()

  const userList = useSelector<rootState, IUser[]>(state => state.userList)
  const current = useSelector((state: rootState) => state.auth)

  const [showPass, setShowPass] = useState(false)
  const [email, setEmail] = useState(current?.email ?? '')
  const [password, setPassword] = useState(current?.password ?? '')

  const dispatch = useDispatch()

  return (
    <SafeAreaView style={[Layout.fill]}>
      {/* <Spacer height={20} /> */}
      <View style={[Layout.fill, Common.backgroundPrimary]}>
        <ScrollView contentContainerStyle={[Gutters.padding16]}>
        {/* <Text style={Fonts.body}>{JSON.stringify(current)}</Text> */}
          <Spacer height={20} />
          <View>
            <Text style={[Fonts.title, Gutters.margin16B]}>Login</Text>
            <Text style={[Fonts.subtitle]}>User Akan Ototmatis dibuat</Text>
            <Spacer height={12} />
            <Text style={[Fonts.bodyBold]}>Email</Text>
            <Spacer height={8} />
            <InputBorder
              placeholder="example@email.com"
              value={email}
              onChangeText={setEmail}
            />
            <Spacer height={12} />
            {/* <Text style={[Fonts.bodyBold]}>Password</Text>
            <Spacer height={8} />
            <InputBorder
              placeholder="Password"
              secureTextEntry={!showPass}
              value={password}
              onChangeText={setPassword}
              suffix={
                <MaterialIcons
                  name={showPass ? 'visibility-off' : 'visibility'}
                  size={20}
                  color={Colors.colorText.secondary}
                  style={Gutter.padding8H}
                  onPress={() => {
                    setShowPass(v => !v)
                  }}
                />
              }
            /> */}

            <Spacer height={24} />
            <ButtonPrimary
              text="Masuk"
              disabled={email === ''}
              onPress={() => {
                // navigateAndSimpleReset('Main')
                const index = userList.findIndex(v => v.email === email)
                if (true) {
                  dispatch(login(userList[index]))
                  navigateAndSimpleReset('Main')
                  // if (userList[index].password === password) {
                  // } else {
                  //   Snackbar.show({
                  //     text: 'Password tidak sesua',
                  //     backgroundColor: Colors.bgModal,
                  //     textColor: 'white',
                  //     duration: Snackbar.LENGTH_SHORT,
                  //   })
                  // }
                } else {
                  dispatch(
                    addUser({
                      book: [],
                      email: email,
                      password: password,
                      username: email.split('.')[0],
                    }),
                  )
                  navigateAndSimpleReset('Main')
                  Snackbar.show({
                    text: `User ${email} created`,
                    backgroundColor: Colors.bgModal,
                    textColor: 'white',
                    duration: Snackbar.LENGTH_SHORT,
                  })
                }
              }}
            />
            <Spacer height={12} />
          </View>

          <Spacer height={120} />
        </ScrollView>
      </View>
    </SafeAreaView>
  )
}

export default LoginScreen
