/* eslint-disable react-native/no-inline-styles */
import React, { useState, useEffect } from 'react'
import {
  View,
  SafeAreaView,
  Text,
  FlatList,
  Image,
  ActivityIndicator,
} from 'react-native'
import { useTheme } from '@/Hooks'
import { Gutter } from '@/Theme/Gutters'
import MaterialIcons from '@/Components/MaterialIcons'
import { useSelector } from 'react-redux'
import { TouchableOpacity, TouchableWithoutFeedback } from 'react-native-gesture-handler'
import { RootStackScreenProps } from '../../Navigators/utils'
import { rootState } from '@/Store'
import { IBookListbySubject, IWork } from '@/types/Booklistbysubject'
import { API_URL } from '@/Config/api_url'
import DropDownPicker from 'react-native-dropdown-picker'
import { IUser } from '@/types/User'
import {
  useGetBookMysteryQuery,
  useGetBookQuery,
} from '@/Services/modules/getBookSubject'
import useArray from '@/Hooks/useArray'
import useUpdateEffect from '@/Hooks/useUpdateEffect'
import { Pressable } from 'react-native/Libraries/Components/Pressable/Pressable'
import { Button } from 'react-native'

const HomeScreen: React.FC<RootStackScreenProps<'Main'>> = ({
  navigation,
  route,
}) => {
  const { Common, Fonts, Layout, Colors } = useTheme()

  const [facultyOpen, setFacultyOpen] = useState(false)
  const [facultyValue, setFacultyValue] = useState(faculty[0].value)

  const current = useSelector((state: rootState) => state.userList)
  const currentUser = useSelector((state: rootState) => state.auth)

  const [offset, setOffset] = useState(1)
  const queryBook = useGetBookQuery({
    subject: facultyValue,
    limit: 10,
    offset: offset * 10,
  })

  const listBook = useArray<IWork>()

  useUpdateEffect(() => {
    if ((queryBook.data?.request.offset ?? 10) >= listBook.array.length) {
      listBook.addAll(queryBook.data?.works ?? [])
    }
  }, [queryBook.data])

  useEffect(() => {
    navigation.setOptions({
      // headerRightContainerStyle:{[Gutter.padding16]},
      // eslint-disable-next-line react/no-unstable-nested-components
      headerRight: (props) => (
        <TouchableWithoutFeedback
          onPress={() => {
            navigation.push('UserBookListScreen', { user: currentUser })
          }}
          style={[Gutter.margin16]}
        >
          <Text style={[Fonts.bodyBold]}>MY Book</Text>
        </TouchableWithoutFeedback>
      )
    })
  }, [navigation, currentUser])

  return (
    <SafeAreaView style={[Layout.fill, Common.backgroundPrimary]}>
      {/* <Image source={Images.bgEllipse} style={styles.overlay} /> */}
      <View style={[Layout.fill, Gutter.padding16, Gutter.padding4T]}>
        {/* <Text style={Fonts.body}>{JSON.stringify(currentUser)}</Text> */}
        <View style={[{ zIndex: 1000 }]}>
          <DropDownPicker
            open={facultyOpen}
            value={facultyValue}
            items={faculty}
            setValue={v => {
              setFacultyValue(v)
              listBook.clear()
              setOffset(1)
            }}
            setOpen={setFacultyOpen}
            style={[Common.cardBorder]}
            containerStyle={[Common.cardBorder]}
            dropDownContainerStyle={[
              {
                position: 'absolute',
                zIndex: 1000,
              },
              Common.cardBorder,
            ]}
          />
        </View>
        <FlatList
          // ScrollIndicator={false}
          data={listBook.array ?? []}
          numColumns={2}
          onEndReached={() => {
            if (
              !queryBook.isFetching &&
              (queryBook.data?.request.offset ?? 10) / 10 === offset
              // true
            ) {
              setOffset(offset + 1)
            }
          }}
          onEndReachedThreshold={0.2}
          keyExtractor={(_, index) => index.toString()}
          renderItem={({ index, item }) => (
            <View style={[Layout.fill, Gutter.margin4]}>
              <TouchableOpacity
                style={[{ height: 300 }]}
                onPress={() => {
                  navigation.push('BookDetail', { book: item, user: null })
                }}
              >
                <View style={[Common.cardBorder, Layout.fill]}>
                  <Image
                    source={{
                      uri: API_URL.getImg(item.cover_id.toString()),
                      height: 200,
                    }}
                    style={[Common.topRadius(8)]}
                    resizeMode="stretch"
                  />
                  <View
                    style={[Layout.colVCenter, Gutter.padding12, Layout.fill]}
                  >
                    <Text style={[Fonts.title18]} numberOfLines={3}>
                      {item.title}
                    </Text>
                    <Text style={[Fonts.caption10]}>
                      Publish on {item.first_publish_year}
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
          )}
        />
      </View>
      {queryBook.isFetching && (
        <ActivityIndicator
          style={[Layout.absoluteCenter]}
          size={40}
          color={Colors.primary}
        />
      )}
    </SafeAreaView>
  )
}

export default HomeScreen

const faculty = [
  { label: 'Love', value: 'love' },
  { label: 'History', value: 'history' },
  { label: 'Schools', value: 'schools' },
  { label: 'Romance', value: 'romance' },
  { label: 'Families', value: 'families' },
  { label: 'Police', value: 'police' },
]
